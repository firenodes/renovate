module.exports = {
    platform: "gitlab",
    endpoint: "https://gitlab.com/api/v4/",
    assignees: ["theoparis"],
    baseBranches: ["main"],
    labels: ["renovate"],
    extends: ["config:base", "group:allNonMajor"],
    includeForks: true,
    dependencyDashboard: true,
    schedule: ["every weekend"],
};
